
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kdsc/components/drawer/crud.dart';
import 'package:kdsc/pages/homepage.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  
  String eventId;
  String eventName;
  String slot;
  String eventDes;
  String price;
  String date;
  var selectedDate;

  crudMethods crudObj = new crudMethods(); 

  

void datesel(){
  Future<DateTime> selectedDate = showDatePicker(
  context: context,
  initialDate: DateTime.now(),
  firstDate: DateTime(2020),
  lastDate: DateTime(2030),
  builder: (BuildContext context, Widget child) {
    return Theme(
      data: ThemeData.light(),
      child: child,
    );
  },
);
}
  @override

  Widget build(BuildContext context) {
    final format = DateFormat("yyyy-MM-dd");
    return new Scaffold(
      appBar: AppBar(
        title: Center(child: Icon(Icons.home)),
        backgroundColor: Colors.pinkAccent,
        actions: <Widget>[
         /* IconButton(
            icon: Icon(Icons.add),
            onPressed: ( ){
              //addDialog(context);
            },
          )*/
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView(
              children: <Widget>[
               Center(child: Text('Auditorium Bookings',
               style: TextStyle(fontSize: 25.0,
               fontWeight: FontWeight.bold),)),
               DateTimeField(
                 format: format,
                 decoration: InputDecoration(
                   labelText: 'Choose Date'
                 ),
                 onChanged: (DateTime value){
                   setState(() {
                     this.date = value.toString();  
                   });
                 },
                 onShowPicker: (context,currentValue){
                   return showDatePicker(
                     context: context,
                     firstDate: DateTime.now(),
                     initialDate: DateTime.now(),
                     lastDate: DateTime(2021),
                   );
                 },
               ),
                  // Padding(
                  //   padding: const EdgeInsets.only(top: 15,bottom: 20.0),
                  //   child: Row(
                  //     children: <Widget>[
                  //       RaisedButton(
                  //         color: Colors.pink[100],
                  //         child: Text('Select the Date'),
                  //         onPressed: (){
                  //             datesel();
                  //         },
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  Row(
                    children: <Widget>[
                      
                      new DropdownButton<String>(
                       items: <String>['Marriage', 'Party', 'Exhibition'].map((String value) {
                        return new DropdownMenuItem<String>(
                        value: value,
                        child: new Text(value),
                      );
                      }).toList(),
                        onChanged: (value) {
                          setState(() {
                            eventName = value;
                          });
                        },
                        hint: Text(eventName??'Choose Event'),
                        ),
                    ],
                  ),
                   Row(
                    children: <Widget>[
                     
                      new DropdownButton<String>(
                       items: <String>['10am-12pm','12pm-2pm','2pm-4pm','4pm-6pm','6pm-8pm','8pm-10pm', '10pm-12pm'].map((String value) {
                        return new DropdownMenuItem<String>(
                        value: value,
                        child: new Text(value),
                      );
                      }).toList(),
                        onChanged: (value) {
                         setState(() {
                           slot = value;
                           price = '2000';
                         });
                        },
                        hint: Text(slot??'Choose Slot'),
                        ),
                    ],
                  ),
                  SizedBox(height: 5.0),
                  TextField(
                    maxLines: 4,
                    decoration: InputDecoration(hintText: 'Additional Instructions'),
                    onChanged: (value) {
                      this.eventDes = value;
                    },
                  ),
                   SizedBox(height: 15.0),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.end,
                     children: <Widget>[
                       Text('Total Price :',
                       style: TextStyle(
                         color: Colors.black38,
                         fontSize:18.0,

                       ),),
                       Text(price??'0',
                       style: TextStyle(
                         fontWeight: FontWeight.bold,
                         fontSize: 25.0
                       ),),
                     ],
                   ),
                   SizedBox(height: 15.0),
                  RaisedButton(
                    child: Text('Book'),
                        color: Colors.pinkAccent,
                        textColor: Colors.white,
                        elevation: 7.0,
                        onPressed: () {
                          
                          Navigator.push(context, MaterialPageRoute(
                            builder: (context)=>Home()
                          ));
                          Fluttertoast.showToast(
                            msg: 'Booking Confirmed'
                          );
                        }

                  )
                  
              ],
            ),
          ),
       
        ),
        
      )
      
      );
    
  }}