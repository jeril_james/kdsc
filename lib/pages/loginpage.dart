import 'package:flutter/material.dart';
import 'package:kdsc/components/fadeanimation.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:kdsc/components/sharedpreferences.dart';
import 'package:kdsc/pages/homepage.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _email;
  String _password;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: ListView(
          children:<Widget>[
            Container(
              
                height: 400,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/background.png'),
                    fit: BoxFit.fill
                  )
                ),
                
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      left: 30,
                      width: 80,
                      height: 200,
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('images/light-1.png'))
                      ),
                      )
                    ),
                    Positioned(
                      left: 140,
                      width: 80,
                      height: 150,
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('images/light-2.png'))
                      ),
                      )
                    ),
                     Positioned(
                     right: 40,
                     top: 40,
                      width: 80,
                      height: 150,
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('images/clock.png'))
                      ),
                      )
                    ),
                    Positioned(
                      child: Container(
                        margin: EdgeInsets.only(top: 50),
                        child: Center(
                          child: Text("Login",style: TextStyle(color: Colors.white, fontSize: 40, fontWeight: FontWeight.bold),) ,)
                      ),
                    )
                  ]
                  
                ),
            ),Padding(
              padding: EdgeInsets.all(30.0),
              child: Column(
                children: <Widget>[
                  FadeAnimation(1.8, Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromRGBO(143, 148, 251, .2),
                          blurRadius: 20.0,
                          offset: Offset(0, 10)
                        )
                      ]
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                            border: Border(bottom: BorderSide(color: Colors.grey[100]))
                          ),
                          child: TextField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Email or Phone number",
                              hintStyle: TextStyle(color: Colors.grey[400])
                            ),
                            onChanged: (value){
                      setState(() {
                        _email = value;
                      });
                    }
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(8.0),
                          child: TextField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Password",
                              hintStyle: TextStyle(color: Colors.grey[400])
                            ),
                            onChanged: (value){
                      setState(() {
                        _password = value;
                      });
                    },
                    obscureText: true,
                          ),
                        )
                      ],
                    ),
                  )),
                  SizedBox(height: 30,),
                  InkWell(
                     onTap: (){
                          print('abc');
                          FirebaseAuth.instance.signInWithEmailAndPassword(
                                 email: _email,
                                 password: _password
                               ).then((AuthResult authResult) async{
                                 SharedPrefer.setPin(authResult.user.email);
                                 await Navigator.push(context, MaterialPageRoute(builder: (context)=>Home()));
                               })
                        .catchError((e) {
                          print(e);
                        });
                        },
                                      child: FadeAnimation(2, Container(
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(143, 148, 251, 1),
                            Color.fromRGBO(143, 148, 251, .6),
                          ]
                        )
                      ),
                      child: Center(
                        
                          child: Text("Login", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)),
                      
                    )),
                  ),
                  SizedBox(height: 70,),
                  FadeAnimation(1.5, Text("Forgot Password?", style: TextStyle(color: Color.fromRGBO(143, 148, 251, 1)),)),
                ],
              ),
            )

          ]
        ),
      ),
    );
  }
}