import 'package:flutter/material.dart';
import 'package:kdsc/components/drawer/profile.dart';
import 'package:kdsc/components/icon_card.dart';
import 'package:kdsc/components/image_cards.dart';
import 'package:kdsc/pages/loginpage.dart';

class Home extends StatefulWidget {
  @override
  
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
     appBar: AppBar(
       title: new Text(""),
        backgroundColor: Colors.transparent,
        
      ),
      drawer: new Drawer(
                  child: new ListView(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top:30.0),
                        child: InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(
                              builder: (context)=>Profile()
                            ));
                          },
                                                  child: new ListTile(
                            title: new Text("Profile"),
                            trailing: new Icon(Icons.person),
                          ),
                        ),
                      ),
                      new ListTile(
                        title: new Text("Bookings"),
                        trailing: new Icon(Icons.ac_unit),
                      ),
                      
                      new ListTile(
                        title: new Text("About-Us"),
                        trailing: new Icon(Icons.assessment),
                      ),
                      new ListTile(
                        title: new Text("Services"),
                        trailing: new Icon(Icons.assignment_ind),
                      ),
                      Divider(),
                      new ListTile(
                        title: new Text("Contact"),
                        trailing: new Icon(Icons.call),
                      ),
                      InkWell(
                        onTap: (){
                          //logout function
                          Navigator.push(context, MaterialPageRoute(
                            builder: (context)=>LoginPage()
                          ));
                        },
                                              child: ListTile(
                          title: Text('Logout'),
                          trailing: Icon(Icons.exit_to_app),                      ),
                      )
                    ],
                  ) ,   
      ),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  
              //    IconButton(icon: Icon(Icons.person_outline, size: 30, color: Colors.black,), onPressed: (){},),
                ],
              ),
              SizedBox(height: 10,),
              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left:8.0),
                  child: RichText(
                    text: TextSpan(children: [
                      TextSpan(text: 'Hello, ', style: TextStyle(fontSize: 32, fontWeight: FontWeight.w700, color: Colors.pinkAccent)),
                      TextSpan(text: 'what are you\nlooking for?')
                    ], style: TextStyle(fontSize: 32, fontWeight: FontWeight.w500, color: Colors.black)),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  IconCard(iconData: Icons.home, text: 'Auditorium',),
                  IconCard(iconData: Icons.pool, text: 'Swimming Pool',),
                  IconCard(iconData: Icons.gamepad , text: 'Basketball',),
                  IconCard(iconData: Icons.transfer_within_a_station , text: 'Football',),
                ],
              ),
              SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left:8.0),
                    child: Text('Best Experiences', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),),

                  ),

                  IconButton(icon: Icon(Icons.more_horiz, color: Colors.black,), onPressed: (){
                     Navigator.push(context, MaterialPageRoute(
                            builder: (context)=>Profile()
                          ));
                  },),

                ],
              ),
              SizedBox(height: 10,),
              Expanded(child: Container(child: ImageCards())),
              SizedBox(height: 25,),

              Align(
                alignment: Alignment.bottomCenter,
                child:  Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(icon: Icon(Icons.home, color: Colors.pink, size: 30,), onPressed: (){
                     
                    }),
                    IconButton(icon: Icon(Icons.search, color: Colors.black, size: 30,), onPressed: (){}),
                    IconButton(icon: Icon(Icons.favorite_border, color: Colors.black, size: 30,), onPressed: (){ 
                       
                       }),
                    IconButton(icon: Icon(Icons.person_outline, color: Colors.black, size: 30,), onPressed: (){
                     Navigator.push(context, MaterialPageRoute(
                            builder: (context)=>Profile()
                          ));
                    }),

                  ],
                )
                ,
              )
            ],
          ),
        ),
      ),
    );
  }
}
