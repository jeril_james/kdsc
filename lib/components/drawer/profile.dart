import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Account'),
        backgroundColor: Colors.pinkAccent,
      ),
      body: Center(
        child: Container(
          child: Column(
            children: <Widget>[
      FittedBox(
      fit: BoxFit.scaleDown,
      child: SizedBox(
      height: 100,
      width: 100,
      child: RaisedButton(
      child: Image.asset(
          'images/face3.jpg'),
          shape: StadiumBorder(),
          //color: Colors.white,
          onPressed: () {},
                 ),
             ),
         ),
              Text('Sample',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold
              ),),
              Text('9988775544',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold
              ),),
              Text('Sample@gmail.com',
              style: TextStyle(
                fontSize: 18.0,
              ),),
              Divider(),
              Container(
                color: Colors.pink,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('My Bookings',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                      color: Colors.white
                    ),)
                  ],
                ),
              ),
              Container(
                color: Colors.black12,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text('Auditorium',
                      style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold
                      ),),
                      Text('04/02/2020',
                       style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold
                      ),),
                      Text('6 pm - 8 pm',
                       style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold
                      ),),
                    ],
                  ),
                ),
              ),
              Divider(),
              Container(
                color: Colors.black12,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text('Swimming Pool',
                      style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold
                      ),),
                      Text('05/02/2020',
                       style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold
                      ),),
                      Text('3 pm - 5 pm',
                       style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold
                      ),),
                    ],
                  ),
                ),
              ),
              Divider(),
            ],
          ),
        ),
      ),
    );
  }
}