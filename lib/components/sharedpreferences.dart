import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefer{
  static Future<void> setPin(String pin)async{
    SharedPreferences preference = await SharedPreferences.getInstance();
    preference.setString('pin', pin);
  }
  static Future<String>getPin() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString('pin');
  }
}