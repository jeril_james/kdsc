import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kdsc/components/images_card.dart';


class ImageCards extends StatefulWidget {
  @override
  _ImageCardsState createState() => _ImageCardsState();
}

class _ImageCardsState extends State<ImageCards> {

  Future<List<DocumentSnapshot>> data;
  Future<List<DocumentSnapshot>> getFeature() async{
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection('categories').getDocuments();
    return qn.documents;
  }

  @override
  void initState(){
    super.initState();
    data = getFeature();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
        child: FutureBuilder<List<DocumentSnapshot>>(
          future: data,
          builder: (_, snapshot){
            if(snapshot.connectionState == ConnectionState.waiting){
              return LinearProgressIndicator();
            }
            else{
              return ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: snapshot.data.length,
              itemBuilder: (_, index) { 
                return ImageCard(
                  snapshot.data[index]
                );
              });
            }
          },
        ),
        // 
            );
  }
}


