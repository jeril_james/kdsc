import 'package:flutter/material.dart';
import 'package:kdsc/components/sharedpreferences.dart';
import 'package:kdsc/pages/homepage.dart';
import 'package:kdsc/pages/loginpage.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void  initState(){
    super.initState();
    pinCheck();
  }

  Future<void>pinCheck() async{
    String pin = await SharedPrefer.getPin();
    if(pin==null){
       Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context)=>LoginPage()
      ));
    }
    else{
      Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context)=>Home()
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Text('KDSC',
      style: TextStyle(
        color: Colors.red,
        fontSize: 25.0
      ),)),
    );
  }
}