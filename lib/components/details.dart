import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kdsc/pages/dashboard.dart';



class Details extends StatefulWidget {
  final DocumentSnapshot documentSnapshot;
  Details({
    this.documentSnapshot
  });
  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 300,
                child: ClipRRect(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30),),
    child: Image.network(widget.documentSnapshot.data['image'], height: 400, width: double.infinity, fit: BoxFit.fill,),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(14.0),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.calendar_today, color: Colors.grey, size: 20,),
                      SizedBox(width: 5,),
                      Text(widget.documentSnapshot.data['days'].toString() +' days', style: TextStyle(color: Colors.grey))
                    ],
                  )
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RichText(
                    text: TextSpan(children: [
                      TextSpan(text: widget.documentSnapshot.data['name'] +'\n', style: TextStyle(fontSize: 22, fontWeight: FontWeight.w700, color: Colors.black)),
                      TextSpan(text: 'KDSC Auditorium is equipped with centralized A/C and high quality Flooring spread over a vast area in the heart of the city, with a seating capacity of 1000 people. The auditorium has an outstanding feature of well maintained and beautiful stage. ',)
                    ], style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500, color: Colors.grey)),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 80,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30)),
                    color: Colors.pink[400],

                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left:10),
                      child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(text: 'Price : \n', style: TextStyle(fontSize: 18)),
                              TextSpan(text: '1000/Hr', style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold))

                            ]
                          ),
                        ),
                    ),
                      Padding(
                        padding: const EdgeInsets.only(right:10.0),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                          
                            child: InkWell(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (_)=>DashboardPage()));
                              },
                              child: Text('Book', style: TextStyle(color: Colors.pink, fontSize: 20),)),
                          ),
                          
                        ),
                      ),
                      
                    ],
                  ),
                ),
              )
            ],
          ),
          Positioned(
            top: 20,
            left: 10,
            child:  GestureDetector(
              onTap: (){
                Navigator.pop(context);
              },
              child: Align(
                alignment: Alignment.topLeft,
                child: Icon(Icons.arrow_back_ios),
              ),
            ),
          )
        ],
      ),
    );
  }
}
